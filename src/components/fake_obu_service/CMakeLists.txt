CMAKE_MINIMUM_REQUIRED(VERSION 3.16)

### source
FILE(GLOB FAKE_OBU_SERVICE_SRCS "*.cpp" "*.cc" "*.c")

### lib
ADD_LIBRARY(air_fake_obu_service OBJECT ${FAKE_OBU_SERVICE_SRCS})
TARGET_LINK_LIBRARIES(air_fake_obu_service
  air_grpc_pb
  air-link-asn-wrapper-grpc_pb
  air-link-asn-wrapper-asn_2
  air-link-asn-wrapper-asn_3
)
