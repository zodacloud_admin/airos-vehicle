CMAKE_MINIMUM_REQUIRED(VERSION 3.16)

### sub directory
ADD_SUBDIRECTORY(config)
ADD_SUBDIRECTORY(base-headers)
ADD_SUBDIRECTORY(crypto-wrapper)
ADD_SUBDIRECTORY(asn-wrapper)
ADD_SUBDIRECTORY(codec)
ADD_SUBDIRECTORY(proto)
ADD_SUBDIRECTORY(components)
ADD_SUBDIRECTORY(framework)
ADD_SUBDIRECTORY(util)
ADD_SUBDIRECTORY(net)
ADD_SUBDIRECTORY(scene)
ADD_SUBDIRECTORY(pc5)

### lib
ADD_LIBRARY(air-link SHARED
    $<TARGET_OBJECTS:air_config>
    $<TARGET_OBJECTS:air_framework>
    $<TARGET_OBJECTS:air_grpc_pb>
    $<TARGET_OBJECTS:air_net_udp>
    $<TARGET_OBJECTS:air_fake_obu_rsu_codec>
    $<TARGET_OBJECTS:air_fake_obu_service>
    $<TARGET_OBJECTS:air_scene_service>
    $<TARGET_OBJECTS:air_scene>
    $<TARGET_OBJECTS:air_api_service>
    $<TARGET_OBJECTS:air_pc5>
    $<TARGET_OBJECTS:air_pc5_service>
    air_link.cc
)
TARGET_LINK_LIBRARIES(air-link
    air_grpc_pb
    air-link-asn-wrapper-grpc_pb
    air-link-asn-wrapper-asn_2
    air-link-asn-wrapper-asn_3
)

### exec
ADD_EXECUTABLE(air-link-framework main.cc)
TARGET_LINK_LIBRARIES(air-link-framework
    air-link
)

### install file/dir
INSTALL(TARGETS
    air-link-framework
    air-link
    air-link-asn-wrapper-grpc_pb
    air-link-asn-wrapper-asn_2
    air-link-asn-wrapper-asn_3
    LIBRARY DESTINATION ${FRAMEWORK_LIB_DIR}
    ARCHIVE DESTINATION ${FRAMEWORK_LIB_DIR}
    RUNTIME DESTINATION ${FRAMEWORK_BIN_DIR}
)
