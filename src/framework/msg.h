/******************************************************************************
 * Copyright 2022 The Airos Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#pragma once
#include <cstdint>
#include <string>

namespace air {
namespace link {

class Msg final {
 public:
  Msg() : type_("TEXT") {}
  Msg(const std::string& data) : type_("TEXT"), data_(data) {}

  /**
   * @brief 获得消息源地址
   * @note 来源：actor/worker/timer
   * @return const std::string& 源地址
   */
  const std::string& GetSrc() const { return src_; }
  const std::string& GetDst() const { return dst_; }

  /**
   * @brief 消息类型
   * @note 目前使用到的 "TEXT", "TIMER";
   * 也可以自定义，用于区分传递给同一个actor的不同消息类型
   * @return const std::string& 消息类型
   */
  const std::string& GetMsgType() const { return type_; }

  /**
   * @brief 消息描述
   * @note 目前air_link timer使用到该函数，见 MyActor::Timeout()
   *
   * @return const std::string& 消息描述
   */
  const std::string& GetMsgDesc() const { return desc_; }

  /**
   * @brief 数据
   *
   * @return const std::string& 数据
   */
  const std::string& GetData() const { return data_; }

  void SetSrc(const std::string& src) { src_ = src; }
  void SetDst(const std::string& dst) { dst_ = dst; }
  void SetMsgType(const std::string& type) { type_ = type; }
  void SetMsgDesc(const std::string& desc) { desc_ = desc; }
  void SetData(const char* data, unsigned int len);
  void SetData(const std::string& data);

 private:
  std::string src_;
  std::string dst_;
  std::string type_;
  std::string desc_;
  std::string data_;
};

}  // namespace link
}  // namespace air
