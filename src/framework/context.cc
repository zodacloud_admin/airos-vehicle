/******************************************************************************
 * Copyright 2022 The Airos Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "framework/context.h"

#include <assert.h>

#include <sstream>
#include <string>

#include "framework/actor.h"
#include "framework/app.h"
#include "framework/log.h"
#include "framework/msg.h"

namespace air {
namespace link {

Context::Context(std::shared_ptr<App> app, std::shared_ptr<Actor> actor)
    : app_(app), actor_(actor), in_worker_(false), in_wait_que_(false) {}

std::shared_ptr<App> Context::GetApp() { return app_.lock(); }

int Context::SendMsg(std::shared_ptr<Msg> msg) {
  if (nullptr == msg) return -1;
  send_.emplace_back(msg);
  return 0;
}

int Context::Init(const char* param) {
  actor_->SetContext(shared_from_this());
  return actor_->Init(param);
}

void Context::Proc(const std::shared_ptr<const Msg>& msg) { actor_->Proc(msg); }

std::string Context::Print() {
  std::stringstream ss;
  ss << "context " << actor_->GetActorName() << ", in worker: " << in_worker_
     << ", in wait queue: " << in_wait_que_;
  return ss.str();
}

}  // namespace link
}  // namespace air
