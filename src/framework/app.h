/******************************************************************************
 * Copyright 2022 The Airos Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#pragma once
#include <atomic>
#include <list>
#include <memory>
#include <mutex>
#include <string>
#include <unordered_map>
#include <vector>

#include <json/json.h>

struct epoll_event;

namespace air {
namespace link {

class Context;
class Msg;
class Actor;
class Event;
class Worker;
class WorkerCommon;
class WorkerTimer;
class ModManager;
class ContextManager;
class WorkerManager;
class EventConn;
class EventConnManager;
class App final : public std::enable_shared_from_this<App> {
  friend class Actor;
  friend class Context;

 public:
  App();
  virtual ~App();

  bool Init();

  bool LoadModsFromConf(const std::string& path);

  bool AddActor(const std::string& inst_name, const std::string& params,
                std::shared_ptr<Actor> actor);
  bool AddWorker(const std::string& inst_name, std::shared_ptr<Worker> worker);

  std::shared_ptr<Msg> SendRequest(const std::string& name,
                                   std::shared_ptr<Msg> msg);

  std::unique_ptr<ContextManager>& GetContextManager() { return context_mgr_; }
  std::unique_ptr<ModManager>& GetModManager() { return mods_; }

  bool AddEvent(std::shared_ptr<Event> ev);
  bool DelEvent(std::shared_ptr<Event> ev);

  int Exec();

 private:
  bool CreateContext(const std::string& mod_name, const std::string& actor_name,
                     const std::string& instance_name,
                     const std::string& params);
  bool CreateContext(std::shared_ptr<Actor> mod_inst,
                     const std::string& params);

  std::shared_ptr<WorkerTimer> GetTimerWorker();

  bool LoadActorFromLib(const Json::Value& root, const Json::Value& actor_list,
                        const std::string& actor_name);
  bool LoadActorFromClass(const Json::Value& root,
                          const Json::Value& actor_list,
                          const std::string& actor_name);
  bool LoadWorkerFromLib(const Json::Value& root,
                         const Json::Value& worker_list,
                         const std::string& worker_name);
  bool LoadWorkerFromClass(const Json::Value& root,
                           const Json::Value& worker_list,
                           const std::string& worker_name);

  /// worker
  bool StartCommonWorker(int worker_count);
  bool StartTimerWorker();

  /// 通知执行事件
  void CheckStopWorkers();

  /// 分发事件
  void DispatchMsg(std::list<std::shared_ptr<Msg>>* msg_list);
  void DispatchMsg(std::shared_ptr<Context> context);
  void ProcessEvent(struct epoll_event* evs, int ev_count);
  void ProcessWorkerEvent(std::shared_ptr<WorkerCommon>);
  void ProcessTimerEvent(std::shared_ptr<WorkerTimer>);
  void ProcessUserEvent(std::shared_ptr<Worker>);
  void ProcessEventConn(std::shared_ptr<EventConn>);

  /// 退出标志
  std::atomic_bool quit_ = {true};
  /// epoll文件描述符
  int epoll_fd_;
  /// 句柄管理对象
  std::unique_ptr<ContextManager> context_mgr_;
  /// 模块管理对象
  std::unique_ptr<ModManager> mods_;
  /// 线程管理对象
  std::unique_ptr<WorkerManager> worker_mgr_;
  /// 与框架通信管理对象
  std::unique_ptr<EventConnManager> ev_conn_mgr_;
  std::mutex dispatch_mtx_;
};

}  // namespace link
}  // namespace air
