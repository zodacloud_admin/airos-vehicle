/******************************************************************************
 * Copyright 2022 The Airos Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "framework/worker.h"

#include <assert.h>
#include <sys/socket.h>
#include <sys/types.h>

#include "framework/cutils.h"
#include "framework/common.h"
#include "framework/log.h"

namespace air {
namespace link {

Worker::Worker() : posix_thread_id_(-1), runing_(false) { CreateSockPair(); }

Worker::~Worker() { CloseSockPair(); }

const std::string Worker::GetWorkerName() const {
  return "worker." + worker_name_ + "." + inst_name_;
}

void Worker::Start() {
  int res = 0;
  if (runing_ == false) {
    runing_ = true;
    res = pthread_create(&posix_thread_id_, NULL, &Worker::ListenThread, this);
    if (res != 0) {
      runing_ = false;
      LOG(ERROR) << "pthread create failed";
      return;
    }
    res = pthread_detach(posix_thread_id_);
    if (res != 0) {
      runing_ = false;
      LOG(ERROR) << "pthread detach failed";
      return;
    }
  }
}

void Worker::Stop() {
  runing_ = false;
  WorkerCmd cmd = WorkerCmd::QUIT;
  SendCmdToMain(cmd);
}

void* Worker::ListenThread(void* obj) {
  Worker* t = static_cast<Worker*>(obj);
  t->OnInit();
  while (t->runing_) t->Run();
  t->OnExit();
  return nullptr;
}

int Worker::SendCmdToMain(const WorkerCmd& cmd) {
  char cmd_char = static_cast<char>(cmd);
  return write(sockpair_[0], &cmd_char, 1);
}

int Worker::SendCmdToWorker(const WorkerCmd& cmd) {
  char cmd_char = static_cast<char>(cmd);
  return write(sockpair_[1], &cmd_char, 1);
}

int Worker::RecvCmdFromMain(WorkerCmd* cmd, int timeout_ms) {
  if (timeout_ms < 0) {
    // block
    if (!IsBlock(sockpair_[0])) {
      SetNonBlock(sockpair_[0], false);
    }
  } else if (timeout_ms == 0) {
    // nonblock
    if (IsBlock(sockpair_[0])) {
      SetNonBlock(sockpair_[0], true);
    }
  } else {
    // timeout
    SetSockRecvTimeout(sockpair_[0], timeout_ms);
  }
  char cmd_char;
  int ret = read(sockpair_[0], &cmd_char, 1);
  *cmd = static_cast<WorkerCmd>(cmd_char);
  return ret;
}

int Worker::RecvCmdFromWorker(WorkerCmd* cmd) {
  char cmd_char;
  int ret = read(sockpair_[1], &cmd_char, 1);
  *cmd = (WorkerCmd)cmd_char;
  return ret;
}

void Worker::SendMsg(const std::string& dst, std::shared_ptr<Msg> msg) {
  msg->SetSrc(GetWorkerName());
  msg->SetDst(dst);
  send_.emplace_back(msg);
}

void Worker::PushSendMsgList(std::list<std::shared_ptr<Msg>>* msg_list) {
  ListAppend(&send_, msg_list);
}

int Worker::DispatchMsg() {
  WorkerCmd cmd = WorkerCmd::IDLE;
  SendCmdToMain(cmd);
  return RecvCmdFromMain(&cmd);
}

int Worker::DispatchAndWaitMsg() {
  WorkerCmd cmd = WorkerCmd::WAIT_FOR_MSG;
  SendCmdToMain(cmd);
  return RecvCmdFromMain(&cmd);
}

const std::shared_ptr<const Msg> Worker::GetRecvMsg() {
  if (que_.empty()) {
    return nullptr;
  }
  auto msg = que_.front();
  que_.pop_front();
  return msg;
}

bool Worker::CreateSockPair() {
  int res = -1;
  bool ret = true;

  res = socketpair(AF_UNIX, SOCK_DGRAM, 0, sockpair_);
  if (res == -1) {
    LOG(ERROR) << "Worker create sockpair failed";
    return false;
  }
  ret = SetNonBlock(sockpair_[0], false);
  if (!ret) {
    LOG(ERROR) << "Worker set sockpair[0] block failed";
    return ret;
  }
  ret = SetNonBlock(sockpair_[1], false);
  if (!ret) {
    LOG(ERROR) << "Worker set sockpair[1] block failed";
    return ret;
  }
  return ret;
}

void Worker::CloseSockPair() {
  if (-1 == close(sockpair_[0])) {
    LOG(ERROR) << "Worker close sockpair[0]: " << GetError();
  }
  if (-1 == close(sockpair_[1])) {
    LOG(ERROR) << "Worker close sockpair[1]: " << GetError();
  }
}

}  // namespace link
}  // namespace air
