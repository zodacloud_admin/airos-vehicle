/******************************************************************************
 * Copyright 2022 The Airos Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#pragma once
#include <atomic>
#include <functional>
#include <memory>
#include <mutex>
#include <string>
#include <utility>

namespace air {
namespace net {

class Noncopyable {
 public:
  Noncopyable(const Noncopyable&) = delete;
  Noncopyable& operator=(const Noncopyable&) = delete;

 protected:
  Noncopyable() = default;
  virtual ~Noncopyable() = default;
};

template <typename T>
class Singleton : public Noncopyable {
 public:
  template <typename... Args>
  static T& getInstance(Args&&... args) {
    static std::unique_ptr<T> self{nullptr};
    if (!self) {
      std::unique_lock<std::mutex> uniqueLock(mutexLock);
      if (!self) {
        self.reset(new T(std::forward<Args>(args)...));
      }
    }
    return *self.get();
  }

 private:
  static std::mutex mutexLock;
  static std::unique_ptr<T> self;
};
template <typename T>
std::mutex Singleton<T>::mutexLock;
template <typename T>
std::unique_ptr<T> Singleton<T>::self{nullptr};

}  // namespace net
}  // namespace air
