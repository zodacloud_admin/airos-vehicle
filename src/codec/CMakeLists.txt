CMAKE_MINIMUM_REQUIRED(VERSION 3.16)

### source
FILE(GLOB FAKE_OBU_RSU_CODEC_SRCS "fake_obu_rsu_codec/*.cc")

### lib
ADD_LIBRARY(air_fake_obu_rsu_codec OBJECT ${FAKE_OBU_RSU_CODEC_SRCS})

### test
ADD_EXECUTABLE(fake_obu_rsu_codec_test
    test/fake_obu_rsu_codec_test.cc
    $<TARGET_OBJECTS:air_fake_obu_rsu_codec>
)
TARGET_LINK_LIBRARIES(fake_obu_rsu_codec_test
    gtest_main
    gtest
)
