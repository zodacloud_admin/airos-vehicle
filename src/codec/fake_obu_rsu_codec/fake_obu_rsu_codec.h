/******************************************************************************
* Copyright 2022 The Airos Authors. All Rights Reserved.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*****************************************************************************/
#pragma once
#include <cstdint>
#include <string>

namespace air {
namespace codec {

class FakeObuRsuCodec {
 public:
  enum class MsgType : uint8_t {
    kBsm = 0x1,
    kRsm = 0x2,
    kMap = 0x3,
    kSpat = 0x4,
    kRsi = 0x5,
    kObstacles = 0x6
  };

  static std::string Encode(uint8_t msg_type, const std::string& data);
  static int Decode(
    const std::string& pack,
    uint8_t* msg_type,
    std::string* data);
};

}  // namespace codec
}  // namespace air
