/******************************************************************************
 * Copyright 2022 The Airos Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/
#pragma once

#include <cmath>
#include <mutex>
#include <vector>

#include <glog/logging.h>

#include "v2xpb-asn-bsm.pb.h"
#include "v2xpb-asn-map.pb.h"
#include "v2xpb-asn-position.pb.h"
#include "v2xpb-asn-spat.pb.h"

namespace v2x {
namespace scene {
struct LaneDistance {
  ::v2xpb::asn::MapNode node_;
  int link_id_;
  int lane_id_;
  double distance_;
};
class HvInMap {
 private:
  ::v2xpb::asn::MapNode map_node_;
  int link_index_;
  int lane_index_;
  std::vector<LaneDistance> lane_distance_;

 public:
  static constexpr double kDefaultLaneWidth = 1.5;
  static_assert(kDefaultLaneWidth > 0, "default lane width can't be negative");
  static constexpr double kAzimuthThrehold = M_PI_2;
  const ::v2xpb::asn::MapNode& GetMapNode();
  const ::v2xpb::asn::MapLink& GetMapLink();
  const ::v2xpb::asn::MapLane& GetMapLane();
  double CalculateDistance(const ::v2xpb::asn::Position& point_start,
                           const ::v2xpb::asn::Position& point_end,
                           const ::v2xpb::asn::Position& pose);
  bool IsHvInMap(const ::v2xpb::asn::Map& map,
                 const ::v2xpb::asn::Position& pose, double heading);
  bool IsHvInLink(const ::v2xpb::asn::MapNode& node, int link_id,
                  const ::v2xpb::asn::Position& pose, double heading);
  bool IsHvInLane(const ::v2xpb::asn::MapNode& node, int link_id,
                  int lane_id, const ::v2xpb::asn::Position& pose,
                  double heading);
  bool FindNearestLane();
};
}  // namespace scene
}  // namespace v2x
